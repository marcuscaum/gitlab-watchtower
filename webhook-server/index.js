import express from 'express';
import transforMergeRequest from '../transforms/mergeRequest.js';
import transformCommitMessage from '../transforms/commitMessage.js';
import issues from '../gitlab/issues/index.js';

const app = express();
app.use(express.json());
const { editLabels } = issues;

app.post('/', (req, res) => {
  const { commitMessage, action } = transforMergeRequest(req.body);
  const { project, issueId } = transformCommitMessage(commitMessage);

  const issueParams = { projectName: project, issueId };

  if (action === 'close') {
    return editLabels({ ...issueParams, labels: [] });
  }

  if (action === 'open') {
    return editLabels({ ...issueParams, labels: ['Awaiting Review'] });
  }

  if (action === 'merge') {
    return editLabels({ ...issueParams, labels: ['Staging'] });
  }

  res.sendStatus(200);
});

app.listen(9000, () => console.log('Node.js server started on port 9000.'));
