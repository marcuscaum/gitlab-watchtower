module.exports = {
  env: {
    jest: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
  },
  rules: {
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: [
          '.js',
          '.test.js',
        ],
      },
    },
  },
};
