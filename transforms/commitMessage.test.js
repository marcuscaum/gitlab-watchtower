import commitMessage from './commitMessage';

describe('commitMessage', () => {
  // the commit message should have a substring with the pattern [ <project> # <issue-id> ]

  it('should return empty object if not project and issue is found', () => {
    const testMessage = 'irwin-fe#1245] Test commit message';
    const expected = {};

    expect(commitMessage(testMessage)).toStrictEqual(expected);
  });

  it('should return a project and found id if is found on the commit message', () => {
    const testMessage = '[irwin-fe#12345] Test commit message';
    const expected = {
      project: 'irwin-fe',
      issueId: '12345',
    };

    expect(commitMessage(testMessage)).toStrictEqual(expected);
  });
});
