/* eslint-disable camelcase */

function transformCommitMessage(commitMessage) {
  if (!commitMessage || commitMessage.indexOf('[') < 0 || commitMessage.indexOf('#') < 0) {
    return {};
  }

  const project = commitMessage.substring(commitMessage.indexOf('[') + 1, commitMessage.indexOf('#'));
  const issueId = commitMessage.substring(commitMessage.indexOf('#') + 1, commitMessage.indexOf(']'));

  return {
    project,
    issueId,
  };
}

export default transformCommitMessage;
