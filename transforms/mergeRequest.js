/* eslint-disable camelcase */

function transformMergeRequest(gitlabPayload) {
  const { object_attributes } = gitlabPayload;

  if (!object_attributes) {
    return {};
  }

  return {
    action: object_attributes.action,
    commitMessage: object_attributes.last_commit.title,
  };
}

export default transformMergeRequest;
