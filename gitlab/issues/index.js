import axios from 'axios';
import dotenv from 'dotenv';
import projects from '../config/projects.js';

const { config } = dotenv;
config();

const HOST = 'https://gitlab.com';

axios.defaults.headers['PRIVATE-TOKEN'] = process.env.GITLAB_ACCESS_TOKEN;

const editLabels = async ({ issueId, projectName, labels }) => {
  if (!issueId || !projectName) {
    return;
  }
  await axios.put(`${HOST}/api/v4/projects/${projects[projectName]}/issues/${issueId}`, {
    labels,
  });
};

export default {
  editLabels,
};
